#Compute theoretical variance (Gaussian approx.) for bispectrum multipoles
#-> via exact binning

import numpy as np
from math import *
import numba as nb
import time
from scipy.interpolate import make_interp_spline
twopi3 = (2*np.pi)**3
twopi6 = twopi3**2


#upload 1-loop power spectrum
fname_pell = '../P_ell.txt'

k_th, p0_th, p2_th, p4_th = np.loadtxt(fname_pell, unpack=True)  

#interpolate p_ell

p0_func = make_interp_spline(k_th, p0_th)
p2_func = make_interp_spline(k_th, p2_th)
p4_func = make_interp_spline(k_th, p4_th)

#-------------------------------------------------------
num = [1, 3, 3, 1, 3, 6, 3, 3, 3, 1] #index of unique term
num = np.array(num)

arr = np.zeros((27,3))

arr[0] = [0, 0, 0]

arr[1] = [2, 0, 0]
arr[2] = [0, 2, 0]
arr[3] = [0, 0, 2]

arr[4] = [2, 2, 0]
arr[5] = [2, 0, 2]
arr[6] = [0, 2, 2]

arr[7] = [2, 2, 2]

arr[8] = [4, 0, 0]
arr[9] = [0, 4, 0]
arr[10] = [0, 0, 4]

arr[11] = [4, 2, 0]
arr[12] = [4, 0, 2]
arr[13] = [0, 2, 4]
arr[14] = [0, 4, 2]
arr[15] = [2, 4, 0]
arr[16] = [2, 0, 4]

arr[17] = [4, 2, 2]
arr[18] = [2, 4, 2]
arr[19] = [2, 2, 4]

arr[20] = [4, 4, 0]
arr[21] = [0, 4, 4]
arr[22] = [4, 0, 4]

arr[23] = [4, 4, 2]
arr[24] = [4, 2, 4]
arr[25] = [2, 4, 4]

arr[26] = [4, 4, 4]

l345 = arr

@nb.jit(nopython = True)
def R_PPP(P0Q1, P0Q2, P0Q3, P2Q1, P2Q2, P2Q3, P4Q1, P4Q2, P4Q3, iQ1, iQ2, iQ3, q1z, q2z, res):
  q3z = -q1z-q2z
  mu1 = q1z*iQ1;  mu2 = q2z*iQ2;  mu3 = q3z*iQ3
      
  L0q1 = 1
  L0q2 = 1
  L0q3 = 1
  L2q1 = 0.5*(-1 + 3*mu1**2)
  L2q2 = 0.5*(-1 + 3*mu2**2)
  L2q3 = 0.5*(-1 + 3*mu3**2)
  L4q1 = 0.125*(3 - 30*mu1**2 + 35*mu1**4)
  L4q2 = 0.125*(3 - 30*mu2**2 + 35*mu2**4)
  L4q3 = 0.125*(3 - 30*mu3**2 + 35*mu3**4)

  res[0] = L0q1*L0q1*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[1] = L0q1*L0q1*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[2] = L0q1*L0q1*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[3] = L0q1*L0q1*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[4] = L0q1*L0q1*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[5] = L0q1*L0q1*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[6] = L0q1*L0q1*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[7] = L0q1*L0q1*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[8] = L0q1*L0q1*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[9] = L0q1*L0q1*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[10] = L0q1*L0q1*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[11] = L0q1*L0q1*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[12] = L0q1*L0q1*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[13] = L0q1*L0q1*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[14] = L0q1*L0q1*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[15] = L0q1*L0q1*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[16] = L0q1*L0q1*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[17] = L0q1*L0q1*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[18] = L0q1*L0q1*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[19] = L0q1*L0q1*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[20] = L0q1*L0q1*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[21] = L0q1*L0q1*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[22] = L0q1*L0q1*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[23] = L0q1*L0q1*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[24] = L0q1*L0q1*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[25] = L0q1*L0q1*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[26] = L0q1*L0q1*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[27] = L0q1*L2q1*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[28] = L0q1*L2q1*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[29] = L0q1*L2q1*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[30] = L0q1*L2q1*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[31] = L0q1*L2q1*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[32] = L0q1*L2q1*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[33] = L0q1*L2q1*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[34] = L0q1*L2q1*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[35] = L0q1*L2q1*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[36] = L0q1*L2q1*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[37] = L0q1*L2q1*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[38] = L0q1*L2q1*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[39] = L0q1*L2q1*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[40] = L0q1*L2q1*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[41] = L0q1*L2q1*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[42] = L0q1*L2q1*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[43] = L0q1*L2q1*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[44] = L0q1*L2q1*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[45] = L0q1*L2q1*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[46] = L0q1*L2q1*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[47] = L0q1*L2q1*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[48] = L0q1*L2q1*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[49] = L0q1*L2q1*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[50] = L0q1*L2q1*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[51] = L0q1*L2q1*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[52] = L0q1*L2q1*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[53] = L0q1*L2q1*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[54] = L0q1*L4q1*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[55] = L0q1*L4q1*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[56] = L0q1*L4q1*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[57] = L0q1*L4q1*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[58] = L0q1*L4q1*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[59] = L0q1*L4q1*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[60] = L0q1*L4q1*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[61] = L0q1*L4q1*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[62] = L0q1*L4q1*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[63] = L0q1*L4q1*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[64] = L0q1*L4q1*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[65] = L0q1*L4q1*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[66] = L0q1*L4q1*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[67] = L0q1*L4q1*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[68] = L0q1*L4q1*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[69] = L0q1*L4q1*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[70] = L0q1*L4q1*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[71] = L0q1*L4q1*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[72] = L0q1*L4q1*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[73] = L0q1*L4q1*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[74] = L0q1*L4q1*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[75] = L0q1*L4q1*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[76] = L0q1*L4q1*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[77] = L0q1*L4q1*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[78] = L0q1*L4q1*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[79] = L0q1*L4q1*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[80] = L0q1*L4q1*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[81] = L2q1*L2q1*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[82] = L2q1*L2q1*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[83] = L2q1*L2q1*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[84] = L2q1*L2q1*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[85] = L2q1*L2q1*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[86] = L2q1*L2q1*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[87] = L2q1*L2q1*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[88] = L2q1*L2q1*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[89] = L2q1*L2q1*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[90] = L2q1*L2q1*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[91] = L2q1*L2q1*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[92] = L2q1*L2q1*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[93] = L2q1*L2q1*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[94] = L2q1*L2q1*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[95] = L2q1*L2q1*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[96] = L2q1*L2q1*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[97] = L2q1*L2q1*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[98] = L2q1*L2q1*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[99] = L2q1*L2q1*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[100] = L2q1*L2q1*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[101] = L2q1*L2q1*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[102] = L2q1*L2q1*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[103] = L2q1*L2q1*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[104] = L2q1*L2q1*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[105] = L2q1*L2q1*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[106] = L2q1*L2q1*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[107] = L2q1*L2q1*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[108] = L2q1*L4q1*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[109] = L2q1*L4q1*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[110] = L2q1*L4q1*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[111] = L2q1*L4q1*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[112] = L2q1*L4q1*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[113] = L2q1*L4q1*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[114] = L2q1*L4q1*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[115] = L2q1*L4q1*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[116] = L2q1*L4q1*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[117] = L2q1*L4q1*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[118] = L2q1*L4q1*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[119] = L2q1*L4q1*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[120] = L2q1*L4q1*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[121] = L2q1*L4q1*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[122] = L2q1*L4q1*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[123] = L2q1*L4q1*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[124] = L2q1*L4q1*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[125] = L2q1*L4q1*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[126] = L2q1*L4q1*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[127] = L2q1*L4q1*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[128] = L2q1*L4q1*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[129] = L2q1*L4q1*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[130] = L2q1*L4q1*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[131] = L2q1*L4q1*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[132] = L2q1*L4q1*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[133] = L2q1*L4q1*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[134] = L2q1*L4q1*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[135] = L4q1*L4q1*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[136] = L4q1*L4q1*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[137] = L4q1*L4q1*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[138] = L4q1*L4q1*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[139] = L4q1*L4q1*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[140] = L4q1*L4q1*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[141] = L4q1*L4q1*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[142] = L4q1*L4q1*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[143] = L4q1*L4q1*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[144] = L4q1*L4q1*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[145] = L4q1*L4q1*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[146] = L4q1*L4q1*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[147] = L4q1*L4q1*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[148] = L4q1*L4q1*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[149] = L4q1*L4q1*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[150] = L4q1*L4q1*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[151] = L4q1*L4q1*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[152] = L4q1*L4q1*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[153] = L4q1*L4q1*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[154] = L4q1*L4q1*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[155] = L4q1*L4q1*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[156] = L4q1*L4q1*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[157] = L4q1*L4q1*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[158] = L4q1*L4q1*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[159] = L4q1*L4q1*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[160] = L4q1*L4q1*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[161] = L4q1*L4q1*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[162] = L0q1*L0q2*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[163] = L0q1*L0q2*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[164] = L0q1*L0q2*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[165] = L0q1*L0q2*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[166] = L0q1*L0q2*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[167] = L0q1*L0q2*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[168] = L0q1*L0q2*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[169] = L0q1*L0q2*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[170] = L0q1*L0q2*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[171] = L0q1*L0q2*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[172] = L0q1*L0q2*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[173] = L0q1*L0q2*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[174] = L0q1*L0q2*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[175] = L0q1*L0q2*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[176] = L0q1*L0q2*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[177] = L0q1*L0q2*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[178] = L0q1*L0q2*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[179] = L0q1*L0q2*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[180] = L0q1*L0q2*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[181] = L0q1*L0q2*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[182] = L0q1*L0q2*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[183] = L0q1*L0q2*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[184] = L0q1*L0q2*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[185] = L0q1*L0q2*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[186] = L0q1*L0q2*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[187] = L0q1*L0q2*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[188] = L0q1*L0q2*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[189] = L0q1*L2q2*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[190] = L0q1*L2q2*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[191] = L0q1*L2q2*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[192] = L0q1*L2q2*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[193] = L0q1*L2q2*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[194] = L0q1*L2q2*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[195] = L0q1*L2q2*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[196] = L0q1*L2q2*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[197] = L0q1*L2q2*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[198] = L0q1*L2q2*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[199] = L0q1*L2q2*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[200] = L0q1*L2q2*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[201] = L0q1*L2q2*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[202] = L0q1*L2q2*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[203] = L0q1*L2q2*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[204] = L0q1*L2q2*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[205] = L0q1*L2q2*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[206] = L0q1*L2q2*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[207] = L0q1*L2q2*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[208] = L0q1*L2q2*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[209] = L0q1*L2q2*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[210] = L0q1*L2q2*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[211] = L0q1*L2q2*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[212] = L0q1*L2q2*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[213] = L0q1*L2q2*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[214] = L0q1*L2q2*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[215] = L0q1*L2q2*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[216] = L0q1*L4q2*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[217] = L0q1*L4q2*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[218] = L0q1*L4q2*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[219] = L0q1*L4q2*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[220] = L0q1*L4q2*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[221] = L0q1*L4q2*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[222] = L0q1*L4q2*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[223] = L0q1*L4q2*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[224] = L0q1*L4q2*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[225] = L0q1*L4q2*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[226] = L0q1*L4q2*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[227] = L0q1*L4q2*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[228] = L0q1*L4q2*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[229] = L0q1*L4q2*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[230] = L0q1*L4q2*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[231] = L0q1*L4q2*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[232] = L0q1*L4q2*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[233] = L0q1*L4q2*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[234] = L0q1*L4q2*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[235] = L0q1*L4q2*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[236] = L0q1*L4q2*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[237] = L0q1*L4q2*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[238] = L0q1*L4q2*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[239] = L0q1*L4q2*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[240] = L0q1*L4q2*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[241] = L0q1*L4q2*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[242] = L0q1*L4q2*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[243] = L2q1*L2q2*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[244] = L2q1*L2q2*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[245] = L2q1*L2q2*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[246] = L2q1*L2q2*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[247] = L2q1*L2q2*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[248] = L2q1*L2q2*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[249] = L2q1*L2q2*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[250] = L2q1*L2q2*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[251] = L2q1*L2q2*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[252] = L2q1*L2q2*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[253] = L2q1*L2q2*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[254] = L2q1*L2q2*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[255] = L2q1*L2q2*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[256] = L2q1*L2q2*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[257] = L2q1*L2q2*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[258] = L2q1*L2q2*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[259] = L2q1*L2q2*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[260] = L2q1*L2q2*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[261] = L2q1*L2q2*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[262] = L2q1*L2q2*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[263] = L2q1*L2q2*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[264] = L2q1*L2q2*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[265] = L2q1*L2q2*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[266] = L2q1*L2q2*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[267] = L2q1*L2q2*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[268] = L2q1*L2q2*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[269] = L2q1*L2q2*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[270] = L2q1*L4q2*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[271] = L2q1*L4q2*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[272] = L2q1*L4q2*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[273] = L2q1*L4q2*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[274] = L2q1*L4q2*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[275] = L2q1*L4q2*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[276] = L2q1*L4q2*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[277] = L2q1*L4q2*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[278] = L2q1*L4q2*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[279] = L2q1*L4q2*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[280] = L2q1*L4q2*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[281] = L2q1*L4q2*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[282] = L2q1*L4q2*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[283] = L2q1*L4q2*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[284] = L2q1*L4q2*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[285] = L2q1*L4q2*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[286] = L2q1*L4q2*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[287] = L2q1*L4q2*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[288] = L2q1*L4q2*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[289] = L2q1*L4q2*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[290] = L2q1*L4q2*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[291] = L2q1*L4q2*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[292] = L2q1*L4q2*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[293] = L2q1*L4q2*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[294] = L2q1*L4q2*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[295] = L2q1*L4q2*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[296] = L2q1*L4q2*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[297] = L4q1*L4q2*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[298] = L4q1*L4q2*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[299] = L4q1*L4q2*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[300] = L4q1*L4q2*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[301] = L4q1*L4q2*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[302] = L4q1*L4q2*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[303] = L4q1*L4q2*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[304] = L4q1*L4q2*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[305] = L4q1*L4q2*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[306] = L4q1*L4q2*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[307] = L4q1*L4q2*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[308] = L4q1*L4q2*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[309] = L4q1*L4q2*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[310] = L4q1*L4q2*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[311] = L4q1*L4q2*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[312] = L4q1*L4q2*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[313] = L4q1*L4q2*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[314] = L4q1*L4q2*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[315] = L4q1*L4q2*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[316] = L4q1*L4q2*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[317] = L4q1*L4q2*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[318] = L4q1*L4q2*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[319] = L4q1*L4q2*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[320] = L4q1*L4q2*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[321] = L4q1*L4q2*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[322] = L4q1*L4q2*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[323] = L4q1*L4q2*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[324] = L0q1*L0q3*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[325] = L0q1*L0q3*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[326] = L0q1*L0q3*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[327] = L0q1*L0q3*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[328] = L0q1*L0q3*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[329] = L0q1*L0q3*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[330] = L0q1*L0q3*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[331] = L0q1*L0q3*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[332] = L0q1*L0q3*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[333] = L0q1*L0q3*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[334] = L0q1*L0q3*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[335] = L0q1*L0q3*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[336] = L0q1*L0q3*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[337] = L0q1*L0q3*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[338] = L0q1*L0q3*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[339] = L0q1*L0q3*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[340] = L0q1*L0q3*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[341] = L0q1*L0q3*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[342] = L0q1*L0q3*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[343] = L0q1*L0q3*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[344] = L0q1*L0q3*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[345] = L0q1*L0q3*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[346] = L0q1*L0q3*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[347] = L0q1*L0q3*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[348] = L0q1*L0q3*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[349] = L0q1*L0q3*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[350] = L0q1*L0q3*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[351] = L0q1*L2q3*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[352] = L0q1*L2q3*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[353] = L0q1*L2q3*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[354] = L0q1*L2q3*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[355] = L0q1*L2q3*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[356] = L0q1*L2q3*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[357] = L0q1*L2q3*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[358] = L0q1*L2q3*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[359] = L0q1*L2q3*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[360] = L0q1*L2q3*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[361] = L0q1*L2q3*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[362] = L0q1*L2q3*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[363] = L0q1*L2q3*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[364] = L0q1*L2q3*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[365] = L0q1*L2q3*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[366] = L0q1*L2q3*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[367] = L0q1*L2q3*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[368] = L0q1*L2q3*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[369] = L0q1*L2q3*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[370] = L0q1*L2q3*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[371] = L0q1*L2q3*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[372] = L0q1*L2q3*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[373] = L0q1*L2q3*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[374] = L0q1*L2q3*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[375] = L0q1*L2q3*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[376] = L0q1*L2q3*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[377] = L0q1*L2q3*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[378] = L0q1*L4q3*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[379] = L0q1*L4q3*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[380] = L0q1*L4q3*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[381] = L0q1*L4q3*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[382] = L0q1*L4q3*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[383] = L0q1*L4q3*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[384] = L0q1*L4q3*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[385] = L0q1*L4q3*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[386] = L0q1*L4q3*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[387] = L0q1*L4q3*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[388] = L0q1*L4q3*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[389] = L0q1*L4q3*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[390] = L0q1*L4q3*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[391] = L0q1*L4q3*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[392] = L0q1*L4q3*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[393] = L0q1*L4q3*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[394] = L0q1*L4q3*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[395] = L0q1*L4q3*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[396] = L0q1*L4q3*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[397] = L0q1*L4q3*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[398] = L0q1*L4q3*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[399] = L0q1*L4q3*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[400] = L0q1*L4q3*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[401] = L0q1*L4q3*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[402] = L0q1*L4q3*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[403] = L0q1*L4q3*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[404] = L0q1*L4q3*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[405] = L2q1*L2q3*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[406] = L2q1*L2q3*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[407] = L2q1*L2q3*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[408] = L2q1*L2q3*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[409] = L2q1*L2q3*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[410] = L2q1*L2q3*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[411] = L2q1*L2q3*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[412] = L2q1*L2q3*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[413] = L2q1*L2q3*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[414] = L2q1*L2q3*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[415] = L2q1*L2q3*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[416] = L2q1*L2q3*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[417] = L2q1*L2q3*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[418] = L2q1*L2q3*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[419] = L2q1*L2q3*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[420] = L2q1*L2q3*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[421] = L2q1*L2q3*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[422] = L2q1*L2q3*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[423] = L2q1*L2q3*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[424] = L2q1*L2q3*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[425] = L2q1*L2q3*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[426] = L2q1*L2q3*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[427] = L2q1*L2q3*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[428] = L2q1*L2q3*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[429] = L2q1*L2q3*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[430] = L2q1*L2q3*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[431] = L2q1*L2q3*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[432] = L2q1*L4q3*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[433] = L2q1*L4q3*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[434] = L2q1*L4q3*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[435] = L2q1*L4q3*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[436] = L2q1*L4q3*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[437] = L2q1*L4q3*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[438] = L2q1*L4q3*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[439] = L2q1*L4q3*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[440] = L2q1*L4q3*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[441] = L2q1*L4q3*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[442] = L2q1*L4q3*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[443] = L2q1*L4q3*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[444] = L2q1*L4q3*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[445] = L2q1*L4q3*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[446] = L2q1*L4q3*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[447] = L2q1*L4q3*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[448] = L2q1*L4q3*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[449] = L2q1*L4q3*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[450] = L2q1*L4q3*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[451] = L2q1*L4q3*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[452] = L2q1*L4q3*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[453] = L2q1*L4q3*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[454] = L2q1*L4q3*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[455] = L2q1*L4q3*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[456] = L2q1*L4q3*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[457] = L2q1*L4q3*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[458] = L2q1*L4q3*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3
  res[459] = L4q1*L4q3*L0q1*L0q2*L0q3*P0Q1*P0Q2*P0Q3
  res[460] = L4q1*L4q3*L2q1*L0q2*L0q3*P2Q1*P0Q2*P0Q3
  res[461] = L4q1*L4q3*L0q1*L2q2*L0q3*P0Q1*P2Q2*P0Q3
  res[462] = L4q1*L4q3*L0q1*L0q2*L2q3*P0Q1*P0Q2*P2Q3
  res[463] = L4q1*L4q3*L2q1*L2q2*L0q3*P2Q1*P2Q2*P0Q3
  res[464] = L4q1*L4q3*L2q1*L0q2*L2q3*P2Q1*P0Q2*P2Q3
  res[465] = L4q1*L4q3*L0q1*L2q2*L2q3*P0Q1*P2Q2*P2Q3
  res[466] = L4q1*L4q3*L2q1*L2q2*L2q3*P2Q1*P2Q2*P2Q3
  res[467] = L4q1*L4q3*L4q1*L0q2*L0q3*P4Q1*P0Q2*P0Q3
  res[468] = L4q1*L4q3*L0q1*L4q2*L0q3*P0Q1*P4Q2*P0Q3
  res[469] = L4q1*L4q3*L0q1*L0q2*L4q3*P0Q1*P0Q2*P4Q3
  res[470] = L4q1*L4q3*L4q1*L2q2*L0q3*P4Q1*P2Q2*P0Q3
  res[471] = L4q1*L4q3*L4q1*L0q2*L2q3*P4Q1*P0Q2*P2Q3
  res[472] = L4q1*L4q3*L0q1*L2q2*L4q3*P0Q1*P2Q2*P4Q3
  res[473] = L4q1*L4q3*L0q1*L4q2*L2q3*P0Q1*P4Q2*P2Q3
  res[474] = L4q1*L4q3*L2q1*L4q2*L0q3*P2Q1*P4Q2*P0Q3
  res[475] = L4q1*L4q3*L2q1*L0q2*L4q3*P2Q1*P0Q2*P4Q3
  res[476] = L4q1*L4q3*L4q1*L2q2*L2q3*P4Q1*P2Q2*P2Q3
  res[477] = L4q1*L4q3*L2q1*L4q2*L2q3*P2Q1*P4Q2*P2Q3
  res[478] = L4q1*L4q3*L2q1*L2q2*L4q3*P2Q1*P2Q2*P4Q3
  res[479] = L4q1*L4q3*L4q1*L4q2*L0q3*P4Q1*P4Q2*P0Q3
  res[480] = L4q1*L4q3*L0q1*L4q2*L4q3*P0Q1*P4Q2*P4Q3
  res[481] = L4q1*L4q3*L4q1*L0q2*L4q3*P4Q1*P0Q2*P4Q3
  res[482] = L4q1*L4q3*L4q1*L4q2*L2q3*P4Q1*P4Q2*P2Q3
  res[483] = L4q1*L4q3*L4q1*L2q2*L4q3*P4Q1*P2Q2*P4Q3
  res[484] = L4q1*L4q3*L2q1*L4q2*L4q3*P2Q1*P4Q2*P4Q3
  res[485] = L4q1*L4q3*L4q1*L4q2*L4q3*P4Q1*P4Q2*P4Q3

@nb.jit(nopython=True)
def numtri(dk, cf, Nb, iOpen):
    '''Counts the triangles
    '''
    N_T = 0
    ishift = int(cf/dk+1.5*iOpen)
    for i in nb.prange(1, Nb+1):
        for j in nb.prange(1, i+1):
            for l in nb.prange(max(1,i-j+1-ishift), j+1):
                N_T += 1
    return N_T


@nb.jit(nopython=True)
def makebins(dk, cf, Nb, iOpen, Ni, Nj, Nl, Bi, Bj, Bl, GetIdx, Symf):
    b = Nb+1
    ishift = int(cf/dk+1.5*iOpen)
    I = 1
    ni = 0
    nj = 0
    nl = 0
    for i in nb.prange(1, Nb+1):
        ni = cf+(i-1)*dk
        for j in nb.prange(1, i+1):
            nj = cf+(j-1)*dk
            for l in nb.prange( max(1,i-j+1-ishift), j+1 ):
                nl = cf+(l-1)*dk
                Ni[I] = ni
                Nj[I] = nj
                Nl[I] = nl
                Bi[I] = i
                Bj[I] = j
                Bl[I] = l
                GetIdx[i*b*b+j*b+l] = I
                flag = int(not(ni-nj)) + int(not(nj-nl))*2
                Symf[I] = (-5*flag*flag*flag + 24*flag*flag -37*flag + 36)/6
                I += 1

@nb.jit(nopython=True)
def binning(P0table, P2table, P4table, L, Nb, km2, kM2, Sqrt, ISqrt, Bin, GetIdx, Symf, R_PPP_bin, counts):
    Nterm = 27*18
    Res = np.zeros(Nterm)
    b = Nb+1
    S1 = S2 = S3 = 0
    B1 = B2 = B3 = 0
    mult = 1
    I = 0
    norm1 = norm2 = norm3 = 0.
    inorm1 = inorm2 = inorm3 = 0.
    PQ1 = PQ2 = PQ3 = 0.
    symf = 0
    k1zmin = k1zmax = k2zmax = 0
    k3z = 0.
    #count_loop = 0
    for k1x in nb.prange(-L, L+1):
        for k1y in nb.prange(-L, L+1):
            if k1x*k1x+k1y*k1y <= kM2:
                for k2x in nb.prange(-L, L+1):
                    for k2y in nb.prange(-L, L+1):
                        if k2x*k2x+k2y*k2y <= kM2:
                            k1zmin = int(sqrt(max(0, km2-k1x*k1x-k1y*k1y)))
                            k1zmax = int(sqrt(kM2-k1x*k1x-k1y*k1y))+1
                            for k1z in nb.prange(-k1zmax, k1zmax+1):
                                S1 = k1x*k1x+k1y*k1y+k1z*k1z
                                if (km2 <= S1 <= kM2):
                                    P0Q1 = P0table[S1]
                                    P2Q1 = P2table[S1]
                                    P4Q1 = P4table[S1]
                                    
                                    inorm1 = ISqrt[S1]
                                    norm1 = Sqrt[S1]
                                    B1 = Bin[S1]
                                    mult = 1
                                    k2zmax = int(sqrt(kM2-k2x*k2x-k2y*k2y))
                                    for k2z in nb.prange(-k2zmax, k2zmax+1):
                                        S2 = k2x*k2x+k2y*k2y+k2z*k2z
                                        S3 = S1 + S2 + 2*(k1x*k2x + k1y*k2y + k1z*k2z)
                                        if (S2 >= km2) and (km2 <= S3 <= kM2):
                                            P0Q2 = P0table[S2]
                                            P2Q2 = P2table[S2]
                                            P4Q2 = P4table[S2]
                                            
                                            P0Q3 = P0table[S3]
                                            P2Q3 = P2table[S3]
                                            P4Q3 = P4table[S3]
                                            
                                            inorm2 = ISqrt[S2]
                                            inorm3 = ISqrt[S3]
                                            norm2 = Sqrt[S2]
                                            norm3 = Sqrt[S3]
                                            B2 = Bin[S2]
                                            B3 = Bin[S3]
                                            I = GetIdx[B1*b*b+B2*b+B3]
                                            if I:
                                                symf = Symf[I]*mult
                                                counts[I] += symf
                                                
                                                R_PPP(P0Q1, P0Q2, P0Q3, P2Q1, P2Q2, P2Q3, P4Q1, P4Q2, P4Q3, inorm1, inorm2, inorm3, k1z, k2z, Res)
                                                for h in nb.prange(Nterm):
                                                    R_PPP_bin[h, I] += Res[h]*symf
                                        
                                        #count_loop += 1
                                        #if (count_loop%100 == 0):
                                        #  print(count_loop)

#---------------binning phase--------------------------
dk, cf, Nbins = 1, 1, 5

Lbox = 1500.
kf = 2.*np.pi/Lbox

kf2 = kf*kf
Nmax = cf + (Nbins-1.)*dk
kmin = cf-0.5*dk
kmax = Nmax+0.5*dk
km2 = kmin*kmin
kM2 = kmax*kmax
L = int(floor(kmax))
L2 = int(kM2)

print("Counting triangles and making bins...")
N_T = numtri(dk, cf, Nbins, 1)
Ni = np.zeros([N_T+1])
Nj = np.zeros([N_T+1])
Nl = np.zeros([N_T+1])
Bi = np.zeros([N_T+1], dtype=int)
Bj = np.zeros([N_T+1], dtype=int)
Bl = np.zeros([N_T+1], dtype=int)
Symf = np.zeros([N_T+1], dtype=int)

GetIdx = np.zeros([(Nbins+1)**3], dtype=int)

makebins(dk, cf, Nbins, 1, Ni, Nj, Nl, Bi, Bj, Bl, GetIdx, Symf)

Sqrt = np.zeros([L2+1])
ISqrt = np.zeros([L2+1])
Bin = np.zeros([L2+1], dtype=int)

P0table = np.zeros([L2+1])
P2table = np.zeros([L2+1])
P4table = np.zeros([L2+1])

print("Preparing bin tables")
for i in range(1, L2+1):
    Sqrt[i] = sqrt(float(i))
    ISqrt[i] = 1./Sqrt[i]
    
    #-------------------------
    #original code gives 
    # segmentation fault for (dk,cf) = (2,2) for example since
    # Bin[-1] >= Nbins
    
    #Bin[i] = int((Sqrt[i]-cf)/dk + 1.5) #original code
    Bin[i] = min(Nbins, int((Sqrt[i]-cf)/dk + 1.5))
    #-------------------------

    P0table[i] = p0_func(kf*Sqrt[i])
    P2table[i] = p2_func(kf*Sqrt[i])
    P4table[i] = p4_func(kf*Sqrt[i])


counts = np.zeros([N_T+1], dtype=int)

Nterm = 27*18
R_PPP_bin = np.zeros([Nterm, N_T+1]) #binned R_lllll

print("Binning!")
start = time.time()
binning(P0table, P2table, P4table, L, Nbins, km2, kM2, Sqrt, ISqrt, Bin, GetIdx, Symf, R_PPP_bin, counts)
end = time.time()
print(end-start)

R_PPP_bin = R_PPP_bin[:,1:]/counts[None,1:]

#You have to calculate this part ... consider first few triangles for example
sym_mult = np.zeros((len(Ni)-1, 3))
for j in range(0, len(Ni)-1):
  i = j+1
  if Ni[i] == Nj[i] and Nj[i] == Nl[i]:
    sym_mult[j, :] = [2, 2, 2]
  elif Nj[i] == Nl[i]:
    sym_mult[j, :] = [2, 0, 0]
  elif Ni[i] == Nj[i]:
    sym_mult[j, :] = [1, 1, 0]
  else:
    sym_mult[j, :] = [1, 0, 0]  

Ntri = (counts[1:]/Symf[1:])

num_index = []
for i in range(len(num)):
  num_index.append(np.sum(num[0:i]))
num_index.append(27)
num_index = np.array(num_index)

Cll_term = np.zeros((6, 10, len(R_PPP_bin.T)))

for sym_count in [0, 1, 2]:
  for ll in range(6):
    for i in range(10):
      Cll_term[ll, i, :] += np.sum(R_PPP_bin[27*ll+num_index[i]+27*6*sym_count:27*ll+num_index[i+1]+27*6*sym_count, :], axis = 0)*sym_mult[:, sym_count]
      #I simply add all contributions from lgdre_ell_2(mu1) (1 x 27*6) 
      #                                and lgdre_ell_2(mu2) (2 x 27*6) 
      #                                and lgdre_ell_2(mu3) (3 x 27*6) 
      #and finally multiply by sym_mult to check the symmetric factor
    
    #if i == 9 and ll == 5:
    #  print(27*ll+num_index[i+1])

C00_term = Cll_term[0, :, :]*(2*0+1)*(2*0+1)*twopi3/kf**3*(1/Ntri[None, :])
C00 = np.sum(C00_term, axis = 0)

C02_term = Cll_term[1, :, :]*(2*0+1)*(2*2+1)*twopi3/kf**3*(1/Ntri[None, :])
C02 = np.sum(C02_term, axis = 0)

C04_term = Cll_term[2, :, :]*(2*0+1)*(2*4+1)*twopi3/kf**3*(1/Ntri[None, :])
C04 = np.sum(C04_term, axis = 0)

C22_term = Cll_term[3, :, :]*(2*2+1)*(2*2+1)*twopi3/kf**3*(1/Ntri[None, :])
C22 = np.sum(C22_term, axis = 0)

C24_term = Cll_term[4, :, :]*(2*2+1)*(2*4+1)*twopi3/kf**3*(1/Ntri[None, :])
C24 = np.sum(C24_term, axis = 0)

C44_term = Cll_term[5, :, :]*(2*4+1)*(2*4+1)*twopi3/kf**3*(1/Ntri[None, :])
C44 = np.sum(C44_term, axis = 0)


#save file
C = np.vstack([C00, C02, C04, C22, C24, C44])
print(np.shape(C))

fname =  'CovB_Gauss_Binned.dat'
print(fname)

np.savetxt(fname, np.transpose(C), header="C00, C02, C04, C22, C24, C44")