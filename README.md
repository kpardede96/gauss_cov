# gauss_cov

-> Generate analytical prediction for power spectrum and bispectrum multipoles variances in the Gaussian approximation.  

Variances are either computed in thin-shell approximation ("_NotBinned") or including the binning effects ("_Binned"). Included also is a sample of one-loop best-fit power spectrum multipoles "P_ell.txt" as the sample input for the code.

Additionally, "EffectiveWavemodes" folder includes notebook to compute effective k's (for the power spectrum) and effective triangles (for the bispectrum). For the effective triangles, sidelengths of the fundamental triangles are sorted before being averaged (see: 1908.01774, section 3.2)
